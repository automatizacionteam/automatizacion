﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automatizacion.Models
{
    public enum DataType
    {
        ReadOnly = 0,
        WriteOnly = 1,
        Both = 2,
        Button = 3
    }
}