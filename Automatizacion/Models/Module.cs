﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automatizacion.Models
{
    public class Module
    {
        public string Name { get; set; }
        public Dictionary<string, string> Data { get; private set; }
        public Dictionary<string, DataType> DataTypes { get; private set; }
        public Dictionary<string , string> Color { get; private set; }
        public Dictionary<string, string> VarName { get; private set; }
        public Module()
        {
            Name = "default";
            Data = new Dictionary<string, string>();
            DataTypes = new Dictionary<string, DataType>();
            Color = new Dictionary<string , string>( );
            VarName = new Dictionary<string, string>();
        }

        public void AddData(string key,string value,DataType type,string name)
        {
            Data.Add(key,value);
            DataTypes.Add(key,type);
            VarName.Add(key,name);
        }

        public void AddData( string key , string value , DataType type, string name, string color )
        {
            Data.Add( key , value );
            DataTypes.Add( key , type );
            VarName.Add(key, name);
            Color.Add( key , color );
        }

        public void RemoveData(string key)
        {
            Data.Remove(key);
            DataTypes.Remove(key);
            VarName.Remove(key);
            Color.Remove(key);
        }
    }
}