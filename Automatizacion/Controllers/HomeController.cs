﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automatizacion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index( )
        {

            //Lectura de sensores y otros elementos
            Random rnd = new Random();
            var Sensors = new List<Automatizacion.Models.Module>();
            var mod = new Models.Module() { Name = "Controles" };
            mod.AddData("Inicio","TRUE", Models.DataType.Button, "MARCHAIN", "success");
            mod.AddData("Parada", "FALSE", Models.DataType.Button, "PARADAIN", "danger");

            //mod.AddData("Sensor1","0", Models.DataType.Both,"NIVEL");

            Sensors.Add(mod);
            var valvula = new Models.Module() { Name = "Actuadores"};
            valvula.AddData("Motor", "FALSE" ,Models.DataType.ReadOnly, "BANDAOUT");
            Sensors.Add(valvula);

            return View( Sensors );
        }

        public ActionResult About( )
        {
            ViewBag.Message = "Your application description page.";

            return View( );
        }

        public ActionResult Contact( )
        {
            ViewBag.Message = "Your contact page.";

            return View( );
        }
    }
}