﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Automatizacion.Startup))]
namespace Automatizacion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
