﻿var ip = "http://172.17.17.20";

var nameToID = {};
var IDToName = {};
var types = {};
var values = {};

/*
	This jscript file is there to update the variables in the table of the variables module on the 
	main page.
 */

var isImplemented = false; // Show whether or not the variable server prg in codesys is running.
var intervalId;
var stopTolerance = 0; // When hitting the start button it might take an additional cycle before the
// symbol information is written to the /ffx/www/variables/mnt/symbols file
// for the first time.

// AJAX GET the new values
function updateVariables() {
    
    $.ajax({
        type: 'GET',
        cache: false,
        dataType: 'TEXT',
        url: (ip + '/myLib.php?vars'),
        success: showVars,
        error: noVars
    });
}

// If successful show the variables. Coordination with the tbale rows et cetera is implicitly done 
// via the /ffx/www/config file.
function showVars(sText) {
    console.log("updating");
    sText = sText.substring(0, sText.search(";;"));
    var vars = sText.split(';');
    for (i = 0; i < vars.length; i++)
    {
        values[IDToName[i]] = vars[i];
        if ($("#" + IDToName[i].replace(".", "")) != null && !($("#" + IDToName[i].replace(".", "")).hasClass('btn'))) $("#" + IDToName[i].replace(".", "")).text(vars[i]);
    }
    stopTolerance = 0;
}

// An error occured getting the variables.
function noVars() {
    if (stopTolerance == 0) {
        clearInterval(intervalId);
        console.log("No Variables");
    }
    else
        stopTolerance -= 1;
}

// Callback for an AJAX get request checking if /ffx/www/variables/mnt/exists is there. That file
// only exists if the cpx variable server program is running.
function yay() {
    isImplemented = true;
}

function aww() {
    isImplemented = false;
    alert('No Connection to PLC');
}

// Send a command to the variables server. See comments in write.php!
function controlServer(cmd) {
    if (isImplemented) {
        $.post((ip+'/myLib.php'), { cmd: cmd });
        if (cmd == 1) {
            stopTolerance = 1;
            var newInterval = 3000;
            if (newInterval >= 500) {
                console.log("interval = " + newInterval);
                clearInterval(intervalId);
                intervalId = setInterval("updateVariables()", newInterval);
            }
        }
    }
}

// Override a click on the close button.
function beforeRemoveVariables() {
    clearInterval(intervalId);
    controlServer(0);
}

/*
	This jscript file is there to write new values from the web interface to the PLC IEC task.
 */

// remember last accessed variable id so that input field can be cleared upon success.
var lastId;

// Writevar simply takes the variable ID (which equals the row number in the html table).
function writeVar(name, type, value) {
    console.log("writting: " + "." + name + ":" + type + "=" + value + ";")
    // transmit the data
    $.post(
		(ip+'/myLib.php'),
		// ie. PLC_PRG.foo:STRING=bar;
		{ set: "."+name + ":" + type + "=" + value + ";" },
		written
	);
}

// Callback for writing the variable;
function written() {
    controlServer(3); // actual write command
    console.log("writting");
}


function loadDB() 
{
    console.log("db loading");
    // Stop the server and create a new dumpfile.
    $.ajax({
        type:'POST',
        cache: false,
        dataType:'TEXT',
        data: { cmd: 0 , dump: 1 },
        url: (ip+'/myLib.php'),
        async: false
    });
				
    // Download the dumpfile and parse the contents.
    $.ajax({
        type:'GET',
        cache: false,
        dataType: 'TEXT',
        url: (ip+'/myLib.php?data'),
        success: parseConfig,
        error: function (data) { console.log(data) },
        async: false
    });
}

function parseConfig(data)
{
    console.log(data);
    var vars = data.split(';');
    for(i=0;i<vars.length;i++)
    {
        var str = vars[i].split(':');
        if (str.length <= 1) break;
        nameToID[str[0]] = i;
        IDToName[i] = str[0];
        types[i] = str[1];
        types[str[0]] = str[1];
    }
}

/*
	The code below parses the hexdump of a binary CoDeSys symbol file. The data is used to 
	display variables in a table. The script generating the hexdump is 
	/ffx/www/variables/dumpSdb. 
	
	Three variables are important to work with the results:
	
	$vars:		The list of variables in the .SDB file. Each list entry is an array containing
					two fields 'type' and 'name'. 'name' is the variable name. 'type' is an integer
					referring to an index in the $types array.
	$types:     A list of types actually used in the .SDB file. The array index equals the file 
					internal index. The values refer to index of the $typeClass array.
	$typeClass: Variable types are enumerated in the .SDB file. The INT obtained from the file 
					can be translated to STRING in this static array.
*/

// These are file internal methods, not that it would hurt anyone to call them.
// But no one needs to really.
var data;

function getUlong(byteNr) {
    return parseInt(data[byteNr / 2 + 1], 16) << 16 | parseInt(data[byteNr / 2], 16)
}

function getChar(byteNr) {
    if (data[Math.floor(byteNr / 2)].substr((byteNr % 2 - 1) * -2, 2) == '00') return '';
    else return unescape("%" + data[Math.floor(byteNr / 2)].substr((byteNr % 2 - 1) * -2, 2));
}

// parseDownloadSdb() is a call back for the AJAX GET request. The parameter
// are the file contents.
function parseDownloadSdb(x) {
    console.log("parsing");
    data = x.split(';');

    // Type class definition from the 3S documentation
    // Unsupported data types are commented out.
    var typeClass = new Array();
    typeClass[0] = 'BOOL';
    typeClass[1] = 'INT';
    typeClass[2] = 'BYTE';
    typeClass[3] = 'WORD';
    typeClass[4] = 'DINT';
    typeClass[5] = 'DWORD';
    typeClass[6] = 'REAL';
    typeClass[7] = 'TIME';
    typeClass[8] = 'STRING';
    //typeClass[9] = 'ARRAY';
    //typeClass[10] = 'ENUM';
    //typeClass[11] = 'USERDEF';
    //typeClass[12] = 'BITORBYTE';
    //typeClass[13] = 'POINTER';
    typeClass[14] = 'SINT';
    typeClass[15] = 'USINT';
    typeClass[16] = 'UINT';
    typeClass[17] = 'UDINT';
    typeClass[18] = 'DATE';
    typeClass[19] = 'TOD';
    typeClass[20] = 'DT';
    //typeClass[21] = 'VOID';
    typeClass[22] = 'LREAL';
    //typeClass[23] = 'REF';
    typeClass[28] = 'LWORD';

    // Global header 
    var headerSize = getUlong(4);

    // Type list header
    var typeListSize = getUlong(headerSize + 4);
    var typeListCount = getUlong(headerSize + 12);

    // Type elements
    var types = new Array();
    var next = headerSize + 16;
    for (i = 0; i < typeListCount; i++) {
        types[i] = getUlong(next + 8);
        next = next + getUlong(next + 4);
    }

    // Var list header 
    next = headerSize + typeListSize;
    var varListSize = getUlong(next + 4);
    var varListCount = getUlong(next + 12);

    // Var elements
    next = next + 16;
    var vars = new Array();
    for (var i = 0; i < varListCount; i++) {
        var tmptype = getUlong(next + 8);
        vars[i] = new Array();
        vars[i][0] = tmptype
        var tmpStr = '';
        var tmpSize = parseInt(data[(next + 24) / 2], 16);
        for (var j = 0; j < tmpSize; j++)
            tmpStr += getChar(next + 26 + j);
        vars[i][1] = tmpStr;
        next = next + getUlong(next + 4);
    }

    // Inject results to document
    var result = '';
    for (var i = 0; i < vars.length; i++) {
        if (typeof (typeClass[types[vars[i][0]]]) != 'undefined' &&  vars[i][1] != 'undefined')
        {
            IDToName[i] = vars[i][1];
            nameToID[vars[i][1]] = i;
            types[i] = typeClass[types[vars[i][0]]];
            types[vars[i][1]] = typeClass[types[vars[i][0]]];
        }
    }

}