﻿var ipCam = "http://172.17.17.25:666/";

function reload() {
    setTimeout(function () {
        window.location.reload(true);
    }, 500);
}

function eureka(data) {
    console.log("Eureka: ");
    console.log(data);
}

function outch(data) {
    console.log("Outch: ");
    console.log(data);
}

function moveUp() {
    $.ajax({
        type: 'GET',
        cache: false,
        url: (ipCam + 'axis-cgi/com/ptz.cgi?camera=1&move=up'),
        success: eureka,
        error: outch
    });
}

function moveDown() {
    $.ajax({
        type: 'GET',
        cache: false,
        dataType: 'TEXT',
        url: ipCam + 'axis-cgi/com/ptz.cgi?camera=1&move=down',
        success: eureka,
        error: outch
    });
}

function moveLeft() {
    $.ajax({
        type: 'GET',
        cache: false,
        dataType: 'TEXT',
        url: ipCam + 'axis-cgi/com/ptz.cgi?camera=1&move=left',
        success: eureka,
        error: outch
    });
}

function moveRight() {
    $.ajax({
        type: 'GET',
        cache: false,
        dataType: 'TEXT',
        url: ipCam + 'axis-cgi/com/ptz.cgi?camera=1&move=right',
        success: eureka,
        error: outch
    });
}

function zoomIn() {
    $.ajax({
        type: 'GET',
        cache: false,
        dataType: 'TEXT',
        url: ipCam + 'axis-cgi/com/ptz.cgi?camera=1&rzoom=2500',
        success: eureka,
        error: outch
    });
}

function zoomOut() {
    $.ajax({
        type: 'GET',
        cache: false,
        dataType: 'TEXT',
        url: ipCam + 'axis-cgi/com/ptz.cgi?camera=1&rzoom=-2500',
        success: eureka,
        error: outch
    });
}